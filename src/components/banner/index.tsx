import { useEffect, useState } from "react";

interface Props {
    imageUrl: string,
    title: string,
    description: string
}

const Banner = (props: Props) => {
    const [scrollPosition, setScrollPosition] = useState(0);

    useEffect(() => {
        console.log(scrollPosition);
        
        const handleScroll = () => {
            setScrollPosition(window.scrollY);
        };

        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        <section className={`jumbotron py-20 bg-cover bg-fixed bg-center flex items-center justify-center relative min-h-screen`} style={{ 
            backgroundImage: `url(${props.imageUrl})`, 
            clipPath: 'polygon(0 0, 100% 0, 100% 85%, 0 100%)',
            backgroundPosition: `50% ${50 + scrollPosition * 0.3}%`
            }}>
            <div className="backdrop absolute top-0 bottom-0 left-0 right-0 bg-[rgba(0,0,0,.4)]" />
            <div className="text-text-primary text-center relative">
                <p className="text-6xl mb-3">{props.title}</p>
                <p className="text-xl">{props.description}</p>
            </div>
        </section>
    );
}

export default Banner;
