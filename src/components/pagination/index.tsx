import { SetStateAction } from "react";
import ReactPaginate from "react-paginate";
import "../../styles/pagination.css"

import { FaChevronRight, FaChevronLeft } from "react-icons/fa6";
import { Post } from "../../feature/posts/models/Post";
import { ResponseMeta } from "../../feature/posts/models/Response";

type PaginationProps = {
    itemsPerPage: number,
    posts: Post[],
    meta: ResponseMeta,
    setFilter: React.Dispatch<SetStateAction<{
        item_per_page: number,
        sort_by: string,
        page_number: number
    }>>
}

const Pagination = ({  meta, setFilter }: PaginationProps) => {
    const pageCount = Math.ceil(meta?.links?.length - 1);

    // Invoke when user click to request another page.
    const handlePageClick = (event: { selected: number; }) => {
        console.log("selected number : ", event.selected);
        
        setFilter(prev => ({ ...prev, page_number: event.selected + 1 }))
    };

    return (
        <>
            <ReactPaginate
                activeClassName={'item active '}
                breakClassName={'item break-me '}
                breakLabel={'...'}
                containerClassName={'pagination'}
                disabledClassName={'disabled-page'}
                marginPagesDisplayed={2}
                nextClassName={"item next "}
                nextLabel={<FaChevronRight />}
                onPageChange={handlePageClick}
                pageCount={pageCount}
                pageClassName={'item pagination-page '}
                pageRangeDisplayed={2}
                previousClassName={"item previous"}
                previousLabel={<FaChevronLeft />}
            />
        </>
    );
}

export default Pagination;
