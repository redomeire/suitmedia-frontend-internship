import { useEffect, useState } from "react"
import { GiHamburgerMenu } from "react-icons/gi";


const headerDatas = [
    {
        name: 'Work',
        url: '/work'
    },
    {
        name: 'About',
        url: '/about'
    },
    {
        name: 'Services',
        url: '/services'
    },
    {
        name: 'Ideas',
        url: '/'
    },
    {
        name: 'Careers',
        url: '/careers'
    },
    {
        name: 'Contact',
        url: '/contact'
    }
]

const Header = () => {
    const [isScrolled, setIsScrolled] = useState(false);
    const [prevScrollY, setPrevScrollY] = useState(0);
    const [isOpenDropdown, setIsOpenDropdown] = useState(false);

    const handleScroll = () => {
        const currentScrollY = window.scrollY;

        if (currentScrollY > prevScrollY) {
            // Scrolling down
            setTimeout(() => {
                setIsScrolled(true);
            }, 300);
        } else if (currentScrollY < prevScrollY) {
            // Scrolling up
            setTimeout(() => {
                setIsScrolled(false);
            }, 300);
        }

        setPrevScrollY(window.scrollY);
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, [window.scrollY]);

    const scrollCondition = isScrolled ? '-translate-y-32' : prevScrollY === 0 ? 'bg-opacity-100' : 'top-0 bg-opacity-90'

    return (
        <header className={`fixed right-0 left-0 bg-orange p-3 md:px-20 px-10 flex justify-between z-20 transition duration-200 ${scrollCondition}`}>
            <div>
                <img src="/images/logo_suitmedia_white.png" width={150} />
            </div>
            <ul className="md:flex items-center gap-4 hidden">
                {
                    headerDatas.map((data, index) => {
                        const isLinkActive = window.location.pathname === data.url

                        return (
                            <li key={index} className={`text-white border-b-2 ${isLinkActive ? 'border-b-white' : 'border-b-transparent'}`}>
                                <a href={data.url}>
                                    {data.name}
                                </a>
                            </li>
                        )
                    })
                }
            </ul>
            <button onClick={() => { setIsOpenDropdown(prev => !prev) }} className="hamburger absolute right-10 top-1/2 -translate-y-1/2 text-white md:hidden ">
                <GiHamburgerMenu size={20} />
            </button>
            <ul className={`mobile flex flex-col p-4 rounded gap-4 md:hidden bg-orange absolute right-0 top-20 w-[200px] transition duration-200 ${!isOpenDropdown ? 'translate-x-[200px]' : 'translate-x-0'}`}>
                {
                    headerDatas.map((data, index) => {
                        const isLinkActive = window.location.pathname === data.url

                        return (
                            <li key={index} className={`text-white border-b-2 ${isLinkActive ? 'border-b-white' : 'border-b-transparent'} w-fit`}>
                                <a href={data.url}>
                                    {data.name}
                                </a>
                            </li>
                        )
                    })
                }
            </ul>
        </header>
    )
}

export default Header;
