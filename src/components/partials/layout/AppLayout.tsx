import React from "react";
import Header from "../header";
import Banner from "../../banner";

interface Props {
    children: React.ReactNode,
    imageUrl: string,
    title: string,
    description: string
}

const AppLayout = (props: Props) => {
    return (
        <>
            <Header />
            <Banner
                title={props.title}
                description={props.description}
                imageUrl={props.imageUrl}
            />
            <main className="md:p-20 p-10">
                {props.children}
            </main>
        </>
    );
}

export default AppLayout;
