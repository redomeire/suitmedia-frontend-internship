import { Post } from "../models/Post";

interface Props {
    post: Post
}

const Post = (props: Props) => {
    return (
        <div className="relative sm:w-64 w-full h-80 overflow-hidden rounded-lg shadow-md hover:shadow-xl transition duration-500 cursor-pointer">
            <img loading="lazy" className="absolute w-full h-full duration-500 object-cover transform scale-100 transition-transform hover:scale-110" src={props.post.medium_image[0]?.url || props.post.small_image[0]?.url} alt="Card Image" />
            <div className="absolute bottom-0 left-0 min-h-[140px] w-full p-4 bg-white rounded-b-lg">
                <h5 className="text-text-secondary font-[500] text-base uppercase">{props.post.created_at}</h5>
                <h3 className="font-semibold text-xl line-clamp-3">{props.post.title}</h3>
            </div>
        </div>
    );
}

export default Post;
