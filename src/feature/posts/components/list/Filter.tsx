import React, { SetStateAction } from "react";
import { ResponseProps } from "../../models/Response";

type filter = {
    item_per_page: number,
    sort_by: string,
    page_number: number
}

interface Props {
    filter: filter
    setFilter: React.Dispatch<SetStateAction<filter>>
    response: ResponseProps
    dataLength?: number
}

const Filter = (props: Props) => {
    const numbers = [10, 20, 50]

    return (
        <div className="filter-container flex justify-end flex-wrap">
            <form className="filter flex gap-5 flex-wrap">
                <div className="flex items-per-page items-center gap-2">
                    <p>show per page: </p>
                    <select
                        onChange={(e) => {
                            props.setFilter(prev => {
                                return { ...prev, item_per_page: parseInt(e.target.value) }
                            })
                        }} className="border rounded-full p-1 cursor-pointer" name="item_per_page" id="item_per_page">
                        {
                            numbers.map((number, index) => {
                                return(
                                    <option key={index} value={number}>{number}</option>
                                )
                            })
                        }
                    </select>
                </div>
                <div className="flex sort-by items-center gap-2">
                    <p>sort by: </p>
                    <select
                        onChange={(e) => {
                            props.setFilter(prev => {
                                return { ...prev, sort_by: e.target.value }
                            })
                        }} className="border rounded-full p-1 cursor-pointer" name="sort_by" id="sort_by">
                        <option value="-published_at">Newest</option>
                        <option value="published_at">Oldest</option>
                    </select>
                </div>
            </form>
        </div>
    )
}

export default Filter;
