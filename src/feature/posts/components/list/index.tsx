import Post from "..";
import { Post as PostType } from "../../models/Post";

interface Props {
    posts: PostType[]
}

const Lists = (props: Props) => {
    return (
        <div className="flex flex-wrap gap-5 justify-center mt-10">
            {
                props.posts?.map((data, index) => {
                    return(
                        <Post post={data} key={index}/>
                    )
                })
            }
        </div>
     );
}
 
export default Lists;
