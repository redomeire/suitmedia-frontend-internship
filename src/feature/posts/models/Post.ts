type image = {
    file_name: string
    id: number
    mime: string
    url: string
}[]

export interface Post {
    content: string
    created_at: string
    deleted_at: string | null
    id: number
    published_at: string
    slug: string
    small_image: image,
    medium_image: image,
    title: string
    updated_at: string
}
