import { Post } from "./Post"

export type ResponseMeta = {
    current_page: number,
    from: number,
    last_page: number,
    links: {
        url: string | null,
        active: boolean,
        label: string
    }[]
}

export interface ResponseProps {
    meta: ResponseMeta,
    data: Post[]
}
