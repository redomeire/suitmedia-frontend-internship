import axios from "axios"

interface Params {
    page_number?: number
    item_per_page: number
    sort_by: string
}

const getPost = async (param: Params) => {
    try {
        const result = await axios.get('/api/ideas', {
            params: {
                'page[number]': param.page_number,
                'page[size]': param.item_per_page,
                'append[]': ['medium_image', 'small_image'],
                'sort': param.sort_by,
            }
        })

        console.log(result);
    
        return result
    } catch (error) {
        console.error(error);
    }
}

export { getPost }
