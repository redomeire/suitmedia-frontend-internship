import { useEffect, useState } from "react";
import Pagination from "../components/pagination";
import AppLayout from "../components/partials/layout/AppLayout";
import Lists from "../feature/posts/components/list";
import Filter from "../feature/posts/components/list/Filter";
import { getPost } from "../feature/posts/services/getPost";
import { ResponseProps } from "../feature/posts/models/Response";
import PageLoader from "../components/loader/PageLoader";

const Home = () => {
    const [filter, setFilter] = useState({
        item_per_page: 10,
        sort_by: '-published_at',
        page_number: 1
    })
    const [response, setResponse] = useState<ResponseProps>({
        meta: {
            current_page: 0,
            from: 0,
            last_page: 0,
            links: [{ active: false, label: '', url: '' }]
        },
        data: []
    })
    const [loading, setLoading] = useState(false)

    const fetchPost = async () => {
        try {
            setLoading(true);

            const result = await getPost({ ...filter });

            setResponse(result?.data);
        } finally {
            setLoading(false);
        }
    };

    useEffect(() => {
        fetchPost()
    }, [filter])

    return (
        <AppLayout title="Ideas" description="where all our great things begin" imageUrl="/images/banner_ideas.jpg">
            <Filter
                filter={filter}
                setFilter={setFilter}
                response={response}
            />
            <Lists posts={response?.data} />
            <Pagination
                posts={response?.data}
                meta={response?.meta}
                setFilter={setFilter}
                itemsPerPage={filter.item_per_page}
            />
            { loading && <PageLoader /> }
        </AppLayout>
    )
}

export default Home;
