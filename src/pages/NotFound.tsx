import AppLayout from "../components/partials/layout/AppLayout";

const NotFound = () => {
    return ( 
        <AppLayout 
            title="Not Found" 
            description="your requested url cannot be found!" 
            imageUrl="/images/banner_notfound.jpg">
            <p>not found</p>
        </AppLayout>
     );
}
 
export default NotFound;
