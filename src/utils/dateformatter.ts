const formatDate = (inputDateString: string) => {
    const inputDate = new Date(inputDateString);

    const formattedDate = inputDate.toLocaleDateString('id-ID', {
        day: 'numeric',
        month: 'long',
        year: 'numeric'
    });

    return formattedDate
}

export { formatDate }
