/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'orange': '#EF6B33',
        'text-primary': '#D1D1D1',
        'text-secondary': '#848484',
        'pale-blue': '#7693CF'
      }
    },
  },
  plugins: [],
}

