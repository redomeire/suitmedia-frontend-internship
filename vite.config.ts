import { defineConfig, loadEnv } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default ({ mode }) => {
  process.env = Object.assign(process.env, loadEnv(mode, process.cwd(), ''));

  return defineConfig({
      plugins: [react()],
      server: {
        proxy: {
          '/api': {
            target: process.env.VITE_APP_API_BASE_URL,
            changeOrigin: true,
            secure: true,
            ws: true,
            rewrite: (path) => path.replace(/^\/api/, ''),
          }
        }
      }
  });
}
